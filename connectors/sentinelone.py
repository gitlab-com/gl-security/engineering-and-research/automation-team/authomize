import os

from authomize.rest_api_client import (
    AccessDescription,
    AccessTypes,
    AssetDescription,
    AssetTypes,
    IdentityDescription,
    IdentityTypes,
)
import requests

from connectors import Base


CONNECTOR_ID = "SentinelOne"

_API_URL_USERS = "https://apne1-1001.sentinelone.net/web/api/v2.1/users"


class Connector(Base):
    """Authomize connector for SentinelOne.

    Access to the API Docs requires being logged in to SentinelOne:
    https://apne1-1001.sentinelone.net/api-doc/overview
    """

    def __init__(self):
        super().__init__(CONNECTOR_ID)
        self.api_token = os.environ.get("SENTINELONE_API_TOKEN")
        if self.api_token is None:
            raise Exception("SentinelOne API Token is missing.")

    def collect(self) -> dict:
        identity, access, asset = self._map_users(self._list_users())
        result = {}
        result[Base.IDENTITY] = identity
        result[Base.ACCESS] = access
        result[Base.ASSET] = asset
        return result

    def _list_users(self):
        """Iterate over user accounts."""
        data = []
        headers = {"Authorization": f"ApiToken {self.api_token}"}
        params = {"limit": 100}
        while data or params:
            if data:
                yield data.pop(0)
            else:
                res = requests.get(_API_URL_USERS, headers=headers, params=params)
                res.raise_for_status()
                body = res.json()
                data = body.get("data", [])
                pagination = body.get("pagination", {})
                cursor = pagination.get("nextCursor")
                if cursor:
                    params["cursor"] = cursor
                else:
                    params = None

    def _map_users(self, users):
        """Map SentinelOne users to Authomize objects.

        Returns a tuple of three lists containing IdentityDescription,
        AccessDescription and AssetDescription objects.
        """
        identity = []
        access = []
        asset = []
        scopes = {}
        for user in users:
            # Map user identity
            identity.append(IdentityDescription(
                id=user["id"],
                name=user["fullName"],
                type=IdentityTypes.User,
                userType=user["scope"],
                email=user["email"],
                createdAt=user["dateJoined"],
                hasTwoFactorAuthenticationEnabled=user["twoFaConfigured"],
                lastLoginAt=user["lastLogin"]
            ))
            # Map user roles
            for role in user.get("scopeRoles", []):
                scope_id = role["id"]
                scope_name = role["name"]
                access.append(AccessDescription(
                    fromIdentityId=user["id"],
                    toAssetId=scope_id,
                    accessType=AccessTypes.Unknown,
                    accessName=role["roleName"]))
                scopes[scope_id] = scope_name
        # Map scopes to assets
        for scope_id, scope_name in scopes.items():
            asset.append(AssetDescription(
                id=scope_id,
                name=f'SentinelOne ({scope_name})',
                type=AssetTypes.Application))
        return identity, access, asset
