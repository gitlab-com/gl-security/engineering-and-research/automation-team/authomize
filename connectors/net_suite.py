"""Authomize connector for NetSuite"""
import os
from typing import Any, Dict, List

from authomize.rest_api_client.generated.connectors_rest_api.schemas import (
    AccessTypes,
    AssetTypes,
    UserStatus,
)
from authomize.rest_api_client import (
    AccessDescription,
    AssetDescription,
    IdentityDescription,
    IdentityTypes,
)
from netsuitesdk import NetSuiteConnection

from connectors import Base
from logger import logger as log

# Arbitrary Asset ID for the NetSuite applicaiton
_ASSET_ID = 1234
_CONNECTOR_ID = "NetSuite"

# Our previous approach to retrieval was doing so from our NetSuite admin since they are not accessible via API.
# There isn't an obvious mapping to the Authomize `AccessTypes` so setting them to unknown allows
# the Authomize UI to show the role as it's named in NetSuite
#
# A _ROLE_MAP of the following form is not necessary:
# {
#     "1": { "name": "Accountant", "type": AccessTypes.Unknown.value },
#     ...
#     "1250": { "name": "Gitlab - Accounting Ops w/ ABR (TESTING ONLY)", "type": AccessTypes.Unknown.value },
# }

# In our previous approach to user and role retrieval,  these were the namespace (the namespace is in brackets) 
# search object necessary to run saved searches (which is necessary to get employees AND their roles) as well as 
# the saved search ID. The namespace was discovered by using an http proxy (Charles/Burpsuite) and running the 
# `netsuitesdk`'s `connector.employees.get_all()` call and then manually creating the request with the `netsuitesdk`.
#
# _EMPLOYEE_SEARCH_ADVANCED = "{urn:employees_2019_1.lists.webservices.netsuite.com}EmployeeSearchAdvanced"
# _SAVED_SEARCH_ID = 671


class Connector(Base):
    """Authomize connector for NetSuite"""

    def __init__(self):
        super().__init__(_CONNECTOR_ID)

    def collect(self) -> dict:
        self.conn = self._authenticate()

        if self.conn is None:
            raise Exception("Connection to NetSuite failed")

        bundle = self._get_users()
        bundle[self.ASSET] = [
            AssetDescription(
                id=_ASSET_ID,
                name="NetSuite",
                type=AssetTypes.Application,
                href="https://netsuite.com",
            )
        ]

        return bundle


    def _authenticate(self):
        account = os.getenv("NS_ACCOUNT")
        client_id = os.getenv("NS_CONSUMER_KEY")
        client_secret = os.getenv("NS_CONSUMER_SECRET")
        token_key = os.getenv("NS_TOKEN_KEY")
        token_secret = os.getenv("NS_TOKEN_SECRET")

        nc = NetSuiteConnection(
            account=account,
            consumer_key=client_id,
            consumer_secret=client_secret,
            token_key=token_key,
            token_secret=token_secret,
            caching=False # Don't create an SQLite DB file
        )

        return nc


    def _get_users(self):
        """Iterate over user accounts."""

        users: Dict[str, List[Any]] = {
            self.ACCESS: [],
            self.IDENTITY: []
        }

        # the employees.get_all method retrieves all 1300+ employees in NetSuite not the
        # 120+ that have roles assigned. However, employees.create_paginated_search simplifies
        # the search and access of all employees

        # listRoles exists, this connector could be simplified further if we got access to that
        # endpoint, but this doesn't seem possible https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/authomize/-/merge_requests/72#note_1413291770 

        user_pages = self.conn.employees.create_paginated_search(50)
        all_users = []

        for page in user_pages:
            all_users.extend(page)

        user_ids = [e.internalId for e in all_users]

        for i in user_ids:
            # individual access of all takes a long time, but contains 
            # role ids and names unlike the saved search approach
            user = self.conn.client.get('employee',i)
            log.info(
                'Processing user',
                extra={
                    'user_id': i,
                    'rolesList': user.rolesList,
                },
            )
            identity = _identity_description(user)
            users[self.IDENTITY].append(identity)
            access_description = _access_description(user)
            users[self.ACCESS].extend(access_description)

        return users

def _identity_description(user) -> IdentityDescription:
    name = user.entityId
    email = user.email
    internal_id = user.internalId

    return IdentityDescription(
        id=internal_id,
        name=name,
        type=IdentityTypes.User.value,
        email=email,
        status=_user_status(user),
    )


def _access_description(user) -> list[AccessDescription]:
    internal_id = user.internalId
    access_descriptions = []
    roles_list = user.rolesList
    if roles_list:
        for role in user.rolesList.roles:
            role = role.selectedRole
            nad =  AccessDescription(
                    fromIdentityId=internal_id,
                    toAssetId=_ASSET_ID,
                    accessType=AccessTypes.Unknown,
                    accessName=role.name,
                )
            access_descriptions.append(nad)

    return access_descriptions

def _user_status(user) -> UserStatus:
    if user.isInactive:
        return UserStatus.Disabled
    return UserStatus.Enabled
