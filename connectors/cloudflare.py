import os
from requests import Session

import jq
from autohelp.gitlab import GitLabAdminHelper

from connectors import Base
from authomize.rest_api_client import (
    AccessDescription,
    AccessTypes,
    AssetDescription,
    AssetTypes,
    IdentityDescription,
    IdentityTypes,
)

CONNECTOR_ID = "Cloudflare"

# the concept of ORGANIZATION will likely be deprecated in the near future
# https://developers.cloudflare.com/api/operations/organizations-(-deprecated)-organization-details
# therefore, rely on accounts

_API_URL_ACCOUNTS = "https://api.cloudflare.com/client/v4/accounts/"
_API_URL_MEMBERS = "https://api.cloudflare.com/client/v4/accounts/{ACCOUNT_ID}/members"

# avoid repeated .get() accesses and manipulations by processing the returned data using JQ
_JQ_QUERY_MEMBERSHIP = """
.[] |
.user.email as $EMAIL |
.user.user.user.two_factor_authentication_enabled as $TWOFACTOR |
.roles |
.[] |
.name as $ROLE |
[$EMAIL, $TWOFACTOR, $ROLE ]
"""
_JQ_QUERY_MEMBERSHIP = jq.compile(_JQ_QUERY_MEMBERSHIP)

_JQ_QUERY_ASSET = """
.[] |
.id as $ID |
.name as $NAME |
.settings.enforce_twofactor as $TWOFACTOR |
[$ID, $NAME, $TWOFACTOR]
"""
_JQ_QUERY_ASSET = jq.compile(_JQ_QUERY_ASSET)


class Connector(Base):
    """Authomize connector for Cloudflare.
    https://developers.cloudflare.com/api/operations/account-members-list-members
    """

    def __init__(self):
        super().__init__(CONNECTOR_ID)

        api_token = os.environ.get("CLOUDFLARE_TOKEN")

        if api_token is None:
            raise Exception("Cloudflare API Token missing")

        self._cloudflare_client = Session()
        self._cloudflare_client.headers = {"Authorization": f"Bearer {api_token}"}
        self._cloudflare_client.params = {'per_page': 50}
        self._gitlab_client = GitLabAdminHelper()

        self._result = {}
        self._result[Base.ASSET] = []
        self._result[Base.IDENTITY] = []
        self._result[Base.ACCESS] = []
        self._processed_objects = []

    def collect(self) -> dict:
        assets = list(self._get_resources(_API_URL_ACCOUNTS))
        assets = _JQ_QUERY_ASSET.input(assets).all()

        # for each Cloudflare account
        for id, name, twofactor in assets:
            # instantiate an AssetDescription
            asset = self._collect_asset(id, name)

            # determine the account-specific members URL
            api_url_members = _API_URL_MEMBERS.format(ACCOUNT_ID=id)

            memberships = list(self._get_resources(api_url_members))
            memberships = _JQ_QUERY_MEMBERSHIP.input(memberships).all()

            # for each account membership create IdentityDescription and
            # AccessDescription
            for email, twofactor, role in memberships:
                identity = self._collect_identity(email, twofactor)
                self._collect_access(asset, identity, role)

        return self._result

    def _get_resources(self, url) -> list:
        """Iterate over user accounts."""
        data = []

        total_pages = 0
        next_page = 1
        end_reached = False

        while True:

            empty = (data is None or len(data) == 0)
            if empty:

                if end_reached:
                    break

                res = self._cloudflare_client.request(
                    'GET', url, params={'page': next_page}
                )
                res.raise_for_status()
                body = res.json()
                data = body.get("result", [])
                result_info = body.get("result_info", {})
                
                total_pages = result_info.get("total_pages")
                if next_page > total_pages:
                    end_reached = True
                next_page += 1

            else:
                yield data.pop(0)

    def _already_processed(self, *args) -> bool:
        """Checks whether the hash of args has been processed and if
        not adds it to self._processed_objects. This function is used
        to check the uniqueness of newly created Authomize objects"""

        fingerprint = hash(str(args))
        if fingerprint in self._processed_objects:
            return True
        else:
            self._processed_objects.append(fingerprint)
            return False

    def _collect_asset(self, id, name) -> AssetDescription:
        """Create and store the requested AssetDescription if it does
        not yet exist, retrieve it and return it otherwise"""
        obj = AssetDescription(
            id=id, name=f'Cloudflare account ({name})', type=AssetTypes.Application
        )
        if not self._already_processed('asset', id, name):
            self._result[Base.ASSET].append(obj)
        return obj

    def _collect_identity(self, email, twofactor) -> IdentityDescription:
        """Create and store the requested IdentityDescription if it does
        not yet exist, retrieve it and return it otherwise"""

        user = self._gitlab_client.find_user(email)
        obj = IdentityDescription(
            id=email,
            name= user.name if user is not None else email,
            type=IdentityTypes.User,
            email=email,
            hasTwoFactorAuthenticationEnabled=twofactor,
        )
        if not self._already_processed('identity', email):
            self._result[Base.IDENTITY].append(obj)
        return obj

    def _collect_access(self, asset, identity, role) -> AccessDescription:
        """Create and store the requested AccessDescription if it does
        not yet exist, retrieve it and return it otherwise"""

        obj = AccessDescription(
            fromIdentityId=identity.id,
            toAssetId=asset.id,
            accessType=AccessTypes.Unknown,
            accessName=role,
        )
        if not self._already_processed('access', asset.id, identity.id, role):
            self._result[Base.ACCESS].append(obj)
        return obj
